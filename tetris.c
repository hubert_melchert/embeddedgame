/*
 * tetris.c
 *
 *  Created on: 23-01-2015
 *      Author: Hubert
 */
#define LEFT 1
#define RIGHT 0
#define DOWN 2
#define ROTATION 3
#include "tetris.h"
uint16_t board[16];
uint16_t boardCopy[16];
Brick brick;
uint8_t points = 0;
uint8_t L1shape[4][4] = {
		{4, 7, 0, 0},
		{3,	2, 2, 0}, // rotated clockwise by 90
		{7, 1, 0, 0},
		{1, 1, 3, 0}
};
uint8_t L2shape[4][4] = {
		{7, 4, 0, 0},
		{3, 1, 1, 0},
		{1, 7, 0, 0},
		{2, 2, 3, 0}
};
uint8_t SQUAREshape[4][4] = {
		{3, 3, 0, 0},
		{3, 3, 0, 0},
		{3, 3, 0, 0},
		{3, 3, 0, 0},
};

uint8_t LINEshape[4][4] = {
		{1, 1, 1, 1},
		{15, 0, 0, 0},
		{1, 1, 1, 1},
		{15, 0, 0, 0},
};
uint8_t TRIANGLEshape[4][4] = {
		{2, 3,2,0},
		{7, 2, 0, 0},
		{1, 3, 1, 0},
		{2, 7, 0, 0}
};
uint8_t WEIRDshape1[4][4] = {
		{2, 3, 1,0},
		{3, 6, 1,0},
		{2, 3, 1,0},
		{3, 6, 1,0}
};
uint8_t WEIRDshape2[4][4] = {
		{1, 3, 2,0},
		{6, 3, 0,0},
		{1, 3, 2,0},
		{6, 3, 0,0},

};
struct myBrick
{
	uint8_t x;
	uint8_t y;
	uint8_t width;
	uint8_t height;
	uint8_t (*shape)[4];
	uint8_t rotation;
};
uint8_t justGenerated = 0;
void tetris()
{
	points = 0;
	button = 0;
	_delay_ms(300);
	clearBoard();
	generateBrick();
	displayBrick();
	uint8_t counter = 0;
	uint8_t rotationReleased = 0;
	while(1)
	{
		if(button == PAUZA)
		{
			frameToBufor_flash(pauza);
			button = 0;
			_delay_ms(500);
			while((PINC & (1<<PC4)));
			_delay_ms(500);
			frameToBufor(board);
			_delay_ms(1000);
		}

		if(button != OK)
		{
			rotationReleased = 1;
		}
		if(button == BLEWY)
		{
			if(checkCollision(LEFT) == 0)
			{
				clearBrick();
				brick.x--;
				displayBrick();
			}
			button = 0;
		}
		else if(button == BPRAWY)
		{
			if(checkCollision(RIGHT) == 0)
			{
				clearBrick();
				brick.x++;
				displayBrick();
				button = 0;
			}
		}
		else if(button == BDOL)
		{
			counter = 7;
			button = 0;
		}
		if(button == OK && rotationReleased == 1)
		{
			button = 0;
			rotationReleased = 0;
			clearBrick();
			rotateBrick();
			displayBrick();
		}
		if(counter == 7)
		{

			if(brick.y+brick.height == 16)
			{
				generateBrick();
				if(checkCollision(ROTATION))
				{
					game_over_slide();
					display_score(points);

					break;
				}
			}
			else justGenerated = 0;

			if(checkCollision(DOWN) == 0 && justGenerated == 0)
			{
				clearBrick();
				brick.y++;
				displayBrick();
			}
			else{
				generateBrick();
				if(checkCollision(ROTATION))
				{
					game_over_slide();
					display_score(points);
					break;
				}
				displayBrick();
			}

			counter = 0;
		}
		else counter++;
		_delay_ms(70);

	}

}

void displayBrick()
{
	for(uint8_t i = 0; i < brick.width; i++)
	{
		board[brick.x + i] |= (brick.shape[brick.rotation][i])<<brick.y;
	}
	frameToBufor(board);
}

void generateBrick()
{
	justGenerated = 1;
	while(checkFullLine());
	for(uint8_t i = 0; i < 16; i++)
	{
		boardCopy[i] = board[i];
	}

	brick.x = 7;
	brick.y = 0;
	brick.rotation = 0;
	uint8_t rand =  TCNT0 % 7;
	switch(rand)
	{
	case 0:
		brick.shape = L1shape;
		brick.height = 3;
		brick.width = 2;
		break;
	case 1:
		brick.shape = L2shape;
		brick.width = 2;
		brick.height = 3;
		break;
	case 2:
		brick.shape = SQUAREshape;
		brick.width = 2;
		brick.height = 2;
		break;
	case 3:
		brick.shape = LINEshape;
		brick.width = 4;
		brick.height = 1;
		break;
	case 4:
		brick.shape = TRIANGLEshape;
		brick.width = 3;
		brick.height = 2;
		break;
	case 5:
		brick.shape = WEIRDshape1;
		brick.width = 3;
		brick.height = 2;
		break;
	case 6:
		brick.shape = WEIRDshape2;
		brick.width = 3;
		brick.height = 2;
		break;
	}
}
void clearBrick()
{

	for(uint8_t i = 0; i < brick.width; i++)
	{
		board[brick.x + i] &= ~((brick.shape[brick.rotation][i])<<brick.y);
	}
}
uint8_t checkFullLine()
{
	uint8_t line = 1;
	for(int8_t y = 15; y >= 0; y--)
	{
		line = 1;
		for(uint8_t x = 4; x < 12; x++) //check vertical line
		{
			if((board[x]>>y & 0x01) == 0)
			{
				line = 0;
				break;
			}
		}
		if(line)
		{
			uint16_t maska = 0;
			for(int8_t i = y-1; i >= 0; i--)
			{
				maska |= (1<<i);
			}
			for(uint8_t x = 4; x < 12; x++)
			{
				board[x] &= ~(1<<y);
			}
			frameToBufor(board);
			_delay_ms(200);

			for(uint8_t i = 4; i < 12; i++)
			{
				boardCopy[i] = (board[i] & maska)<<1;
			}

			for(uint8_t x = 4; x < 12; x++)
			{
				board[x] &= ~(maska);
			}
			frameToBufor(board);

			for(uint8_t x = 4; x < 12; x++)
			{
				board[x] |= boardCopy[x];
			}
			frameToBufor(board);
			points++;
			generate_sound(95,6);
			return 1;
		}
	}
	return 0;
}
void rotateBrick()
{
	brick.rotation++;
	if(brick.rotation == 4)
	{
		brick.rotation = 0;
	}
	if(brick.shape == LINEshape)
	{
		if(brick.rotation == 1 || brick.rotation == 3 )
		{
			brick.x++;
			brick.y--;
		}
		else if(brick.rotation == 2 || brick.rotation == 0)
		{
			brick.x--;
			brick.y++;
		}
	}
	uint8_t swap = brick.width;
	brick.width = brick.height;
	brick.height = swap;
	if(checkCollision(ROTATION) || brick.y+brick.height > 16)
	{
		uint8_t swap = brick.width;
		brick.width = brick.height;
		brick.height = swap;
		if(brick.shape == LINEshape)
		{
			if(brick.rotation == 1 || brick.rotation == 3)
			{
				brick.x--;
				brick.y++;
			}
			else if(brick.rotation == 2 || brick.rotation == 0)
			{
				brick.x++;
				brick.y--;
			}
		}
		if(brick.rotation == 0)
		{
			brick.rotation = 3;
		}else brick.rotation-- ;
	}
}
uint8_t checkCollision(uint8_t direction){
	int8_t v = 0;
	uint8_t point = 0;
	if(direction == LEFT)
	{
		for(uint8_t i = 0; i < brick.width; i++)
		{
			v = 0;
			do
			{

				point = (brick.shape[brick.rotation][i]>>v)&0x01;
				if(point)
				{
					if(((boardCopy[brick.x+i-1]>>(brick.y+v))& 0x01) == 1)
					{
						return 1;
					}
				}
				v++;
			}while(v < brick.height);
		}
	}
	else if(direction == RIGHT)
	{
		for(int8_t i = brick.width - 1; i >= 0; i--)
		{
			v = 0;
			do
			{

				point = (brick.shape[brick.rotation][i]>>v)&0x01;
				if(point)
				{
					if(((boardCopy[brick.x+i+1]>>(brick.y+v))& 0x01) == 1)
					{
						return 1;
					}
				}
				v++;
			}while(v < brick.height);
		}
	}
	else if(direction == DOWN)
	{
		for(int8_t i = brick.width - 1; i >= 0; i--)
		{
			v = brick.height;
			do
			{

				point = (brick.shape[brick.rotation][i]>>v)&0x01;
				if(point)
				{
					if(((boardCopy[brick.x+i]>>(brick.y+v+1))& 0x01) == 1)
					{
						return 1;
					}
				}
				v--;
			}while(v >= 0);
		}
	}
	else if(direction == ROTATION)
	{
		for(int8_t i = brick.width - 1; i >= 0; i--)
		{
			v = brick.height;
			do
			{

				point = (brick.shape[brick.rotation][i]>>v)&0x01;
				if(point)
				{
					if(((boardCopy[brick.x+i]>>(brick.y+v))& 0x01) == 1)
					{
						return 1;
					}
				}
				v--;
			}while(v >= 0);
		}
	}

	return 0;
}
void clearBoard()
{
	for( uint8_t i = 0; i < 16; i++)
	{
		if(i == 3 || i == 12)
		{
			board[i] = 65535;
			boardCopy[i] = 65535;
		}
		else
		{
			board[i] = 0;
			boardCopy[i] = 0;
		}
	}
}
