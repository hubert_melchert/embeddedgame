/*
 * snake_game.h
 *
 *  Created on: 30-11-2014
 *      Author: Hubert
 */

#ifndef SNAKE_GAME_H_
#define SNAKE_GAME_H_

#include <inttypes.h>
#include "main_functions.h"
#include <stdlib.h>
void snake();
uint8_t check_collision();
void move_snake();
void generate_apple();
#endif /* SNAKE_GAME_H_ */
