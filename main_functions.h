/*
 * main_functions.h
 *
 *  Created on: 29-11-2014
 *      Author: Hubert
 */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <setjmp.h>

#define CLOCK PB5
#define DATA PB3
#define LATCH PB2
#define DEBOUNCE_COUNT 80

#define BLEWY 1
#define BPRAWY 2
#define BGORA 3
#define BDOL 4
#define PAUZA 5
#define OK 6

#define c1 479
#define d1 427
#define e1 379
#define f1 358
#define g1 319
#define a1 284
#define h1 253
#define c2 239
#define d2 213
#define e2 189
#define f2 179
#define g2 159
#define fis1 338
#define b1 338
#define es2 201


#ifndef MAIN_FUNCTIONS_H_
#define MAIN_FUNCTIONS_H_

//zmienne

extern const uint16_t word_snake[] PROGMEM;
extern const uint16_t ETI_logo[] PROGMEM;
extern const uint16_t word_pong[] PROGMEM;
extern const uint16_t word_teris[] PROGMEM;
extern const uint16_t pauza[] PROGMEM;
extern const uint8_t win[] PROGMEM;
extern const uint8_t game_over[] PROGMEM;
extern const uint16_t cleanScreen[];
extern const uint16_t play[] PROGMEM;
extern const uint16_t record[] PROGMEM;
extern const uint16_t beep[] PROGMEM;
extern const uint16_t off[] PROGMEM;
extern const uint16_t on[] PROGMEM;
extern const uint16_t star_wars_theme[] PROGMEM;
extern const char digits[][4] PROGMEM;
extern jmp_buf buf;
extern uint8_t animationOn;
void eti_animate();
extern void delay(uint16_t count);
extern volatile uint8_t button;
extern volatile uint16_t sound_duration_timer;
extern uint8_t sound; // 1 ON 0 OFF
extern volatile uint8_t prev_button;
//funkcje
void display_record();
void display_score(uint8_t temp_rekord);
uint8_t menu();
void SPI_MasterInit(void);
void SPI_Send(uint8_t dataByte);
uint8_t check_buttons();
void frameToBufor(const uint16_t frame[]);
void frameToBufor_flash(const uint16_t frame[]);
void slide_right(uint16_t left_frame[], uint16_t right_frame[]);
void slide_left(uint16_t left_frame[], uint16_t right_frame[]);
void EEPROM_write(unsigned int uiAddress, unsigned char ucData);
void voltage();
unsigned char EEPROM_read(unsigned int uiAddress);
void generate_sound(uint16_t period, uint16_t duration);
void modify_sound_state (void);
void you_win_slide();
void game_over_slide();
void play_music(uint16_t music[]);
void chip_url_slide();
#endif /* MAIN_FUNCTIONS_H_ */
