#include "main_functions.h"
#include <avr/interrupt.h>
#include <avr/iom168.h>


const uint16_t word_snake[] PROGMEM = {0,70,73,65097,10289,17408,33407,4,8,65151,37376,37502,33289,9,126,0};
const uint16_t ETI_logo[] PROGMEM = {0, 32766, 32766, 24966, 24966,	24960, 24582, 6, 32766, 32766, 6, 6, 0, 32758, 32758, 0};
const uint16_t pauza[] PROGMEM = {0	,127,	9,	17929,	18694,	18688,	12670,	9,	9	,32638	,18688,	18751,	16704,	64,	63,	0};
const uint16_t play[] PROGMEM = {0,	0,	0,	32383,	2313,	2313,	32262,	0,	256	,639,	31808,	576,	320,	0,	0,	0};
const uint16_t record[] PROGMEM = {0,	15999,	16665,	16681,	15942,	0	,32639	,6473	,10569	,17985	,0,32574,	16705,	16705,	15937	,0};
const uint16_t word_pong[] PROGMEM = {0	,0	,0	,65151,2057,4105,65030,0,0,31806,33345,37441,62014,	0,	0,	0};
const uint16_t word_tetris[] PROGMEM = {1,65025,12927,20993,35841,0,127,73,65097,65,0,35841,37377,37503,25089,1};
const uint16_t battery[] PROGMEM = {0,0,2016,2016,1952,1824,1568,1056,1056,1056,1056,2016,384,0,0,0};
const uint16_t beep[] PROGMEM = {0,0,0,32639,18761,18761,16694,0,0,32639,2377,2377,1601,0,0,0};
const uint16_t off[] PROGMEM = {0,992,1040,1040,992,0,2032,144,144,16,0,2032,144,144,16,0};
const uint16_t on[] PROGMEM = {0,0,0,992,1040,1040,992,0,0,2032,64,128,2032,0,0,0};
const uint16_t beep_on[] PROGMEM = {127, 73, 73, 15926, 16640, 16767, 15945, 73, 32512, 1151, 2121, 32585, 0, 127, 9, 6};
const uint16_t beep_off[] PROGMEM = {127, 15945, 16713, 16694, 15872, 127, 32585, 2377, 2304, 383, 73, 32585, 2304, 2431, 265, 6};
const uint8_t win[] PROGMEM = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,124,2,1,0,62,65,65,62,0,63,64,64,63,0,0,0,0,63,64,48,64,63,0,127,0,127,4,8,127,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
const uint8_t game_over[] PROGMEM = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,62,65,73,121,0,126,9,9,126,0,127,2,4,2,127,0,127,
		73,73,65,0,0,0,62,65,65,62,0,31,32,64,32,31,0,127,73,73,65,0,127,25,41,70,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


const uint16_t eti0[] PROGMEM = {0,0,0,0,0,0,0,65535,65535,0,0,0,0,0,0,0};
const uint16_t eti1[] PROGMEM = {0,0,0,0,0,32769,40569,40953,32769,65529,32777,0,0,0,0,0};
const uint16_t eti2[] PROGMEM = {0,0,65535,32769,40569,40569,40575,65529,32769,32769,65529,65535,32777,65535,0,0};
const uint16_t eti3[] PROGMEM = {0,65535,32769,32769,40569,40575,40953,65529,32769,32769,65529,65529,32777,32777,65535,0};
const uint16_t eti4[] PROGMEM = {65535,32769,32769,40569,40569,40575,40953,65529,32769,32769,65529,65529,65535,32777,32777,65535};

const uint16_t eti0m[] PROGMEM = {0,0,0,0,0,0,0,65535,65535,0,0,0,0,0,0,0};
const uint16_t eti1m[] PROGMEM = {0, 0, 0, 0, 0, 32777, 65529, 32769, 40953, 40569, 32769, 0, 0, 0, 0, 0};
const uint16_t eti2m[] PROGMEM = {0, 0, 65535, 32777, 65535, 65529, 32769, 32769, 65529, 40575, 40569, 40569, 32769, 65535, 0, 0};
const uint16_t eti3m[] PROGMEM = {0, 65535, 32777, 32777, 65529, 65529, 32769, 32769, 65529, 40953, 40575, 40569, 32769, 32769, 65535, 0};
const uint16_t eti4m[] PROGMEM = {65535, 32777, 32777, 65535, 65529, 65529, 32769, 32769, 65529, 40953, 40575, 40569, 40569, 32769, 32769, 65535};
const uint16_t chip_url[] PROGMEM  =
{0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0011100000111100, 0b0100000001000010, 0b0100000010000010, 0b0100000100000010, 0b0011111000011100, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0000000111000000, 0b0000001000100000, 0b0000110000011000, 0b0011000000000100, 0b0100000000000010, 0b0000000000000000, 0b0111111111111110, 0b0000000001111000, 0b0000001110000000, 0b0000110000000000, 0b0111111111111110, 0b0000000000000000, 0b0000000000000000, 0b0011111111111100, 0b0100000000000010, 0b0100000000000010, 0b0100000000000010, 0b0011100000011100, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0000000010000000, 0b0000000010000000, 0b0000000010000000, 0b0111111111111110, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0000000100000010, 0b0000000100000010, 0b0000000100000010, 0b0000000011111100, 0b0000000000000000, 0b0000000000000000, 0b0110000000000000, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0100000010000010, 0b0100000010000010, 0b0100000010000010, 0b0000000000000000, 0b0000000000000010, 0b0000000000000010, 0b0111111111111110, 0b0000000000000010, 0b0000000000000010, 0b0000000000000000, 0b0111111111111110, 0b0000000000000000, 0b0000000000000000, 0b0110000000000000, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0000000100000010, 0b0000000100000010, 0b0000000100000010, 0b0000000011111100, 0b0000000000000000, 0b0000000000000000, 0b0011111111111100, 0b0100000000000010, 0b0100000000000010, 0b0100000100000010, 0b0011111100011100, 0b0000000000000000, 0b0000000000000000, 0b0110000000000000, 0b0000000000000000, 0b0000000000000000, 0b0011111111111100, 0b0100000000000010, 0b0100000000000010, 0b0100000100000010, 0b0011111100011100, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0100000000000010, 0b0100000000000010, 0b0100000000000010, 0b0011111111111100, 0b0000000000000000, 0b0110000000000000, 0b0001111100000000, 0b0000100011111000, 0b0000100000000110, 0b0000100011111000, 0b0001111100000000, 0b0110000000000000, 0b0000000000000000, 0b0110000000000000, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0000000100000010, 0b0000000100000010, 0b0000000100000010, 0b0000000011111100, 0b0000000000000000, 0b0000000000000000, 0b0111111111111110, 0b0100000000000000, 0b0100000000000000, 0b0100000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000, 0b0000000000000000};

uint8_t animationOn = 0;

const uint16_t chip[4][16] PROGMEM = {
		{0b0000000000000000,
				0b0000000000000000,
				0b0000011111100000,
				0b0001111111111000,
				0b0001111111111000,
				0b0011100000011100,
				0b0111000000001110,
				0b0110000000000110,
				0b0110000000000110,
				0b0110000000000110,
				0b0110000000000110,
				0b0110000000000110,
				0b0110000000000110,
				0b0011000000001100,
				0b0000000000000000,
				0b0000000000000000},

				{0b0000000000000000,
						0b0000000000000000,
						0b0111111111111110,
						0b0111111111111110,
						0b0111111111111110,
						0b0000000110000000,
						0b0000000110000000,
						0b0000000110000000,
						0b0000000110000000,
						0b0000000110000000,
						0b0000000110000000,
						0b0111111111111110,
						0b0111111111111110,
						0b0111111111111110,
						0b0000000000000000,
						0b0000000000000000},

						{0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0111111111111110,
								0b0111111111111110,
								0b0111111111111110,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000,
								0b0000000000000000},

								{0b0000000000000000,
										0b0000000000000000,
										0b0000000000000000,
										0b0111111111111110,
										0b0111111111111110,
										0b0111111111111110,
										0b0000001100000110,
										0b0000001100000110,
										0b0000001100000110,
										0b0000001100000110,
										0b0000001110001110,
										0b0000001111111110,
										0b0000000111111100,
										0b0000000011111000,
										0b0000000000000000,
										0b0000000000000000}
};

 const uint16_t eti_zoom[7][16] PROGMEM = {
		{0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000110000000,
		0b0000000110000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000},

		{0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000111000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000},

		{0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000001111000000,
		0b0000001111000000,
		0b0000000000000000,
		0b0000011111000000,
		0b0000000001000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000},

		{0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000111111110000,
		0b0000000000010000,
		0b0000011011010000,
		0b0000011111010000,
		0b0000000000010000,
		0b0000111111010000,
		0b0000111111110000,
		0b0000000000010000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000},

		{0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000,
		0b0001111111111000,
		0b0001000000001000,
		0b0001011001101000,
		0b0001011001111000,
		0b0001111111101000,
		0b0001000000001000,
		0b0001111111101000,
		0b0001111111101000,
		0b0001000000101000,
		0b0001111111111000,
		0b0000000000000000,
		0b0000000000000000,
		0b0000000000000000},

		{0b0000000000000000,
		0b0000000000000000,
		0b0011111111111100,
		0b0010000000000100,
		0b0010111001110100,
		0b0010111001110100,
		0b0010111001111100,
		0b0011111111110100,
		0b0010000000000100,
		0b0010000000000100,
		0b0011111111110100,
		0b0011111111111100,
		0b0010000000010100,
		0b0011111111111100,
		0b0000000000000000,
		0b0000000000000000},

		{0b0000000000000000,
		0b0111111111111110,
		0b0100000000000010,
		0b0100000000000010,
		0b0100111001110010,
		0b0100111001111110,
		0b0100111111110010,
		0b0111111111110010,
		0b0100000000000010,
		0b0100000000000010,
		0b0111111111110010,
		0b0111111111110010,
		0b0100000000010010,
		0b0100000000010010,
		0b0111111111111110,
		0b0000000000000000}
};

//const uint16_t star_wars_theme[] PROGMEM = {d1,200,d1,200,d1,200,g1,1200,d2,1200,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,c2,200,a1,1200,
//		d1,400,d1,200,d1,200,d1,200,g1,1200,d2,1200,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,c2,200,a1,1200,d1,400,d1,200,e1,900,e1,300,
//		c2,300,h1,300,a1,300,g1,300,g1,200,a1,200,h1,200,a1,400,e1,200,fis1,600,d1,400,d1,200,e1,900,e1,300,c2,300,h1,300,a1,300,g1,300,d2,450,a1,150,a1,1200,d1,400,d1,200,e1,900,
//		e1,300,c2,300,h1,300,a1,300,g1,300,g1,200,a1,200,h1,200,a1,400,e1,200,fis1,600,d2,400,d2,200,g2,300,f2,300,es2,300,d2,300,c2,300,b1,300,a1,300,g1,300,d2,1800,d1,400,d1,200,g1,1200,d2,1200,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,c2,200,a1,1200,
//		d1,400,d1,200,d1,200,d1,200,g1,1200,d2,1200,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,a1,200,g2,1200,d2,600,c2,200,h1,200,c2,200,a1,1200,d1,400,d1,200,e1,900,e1,300,
//		c2,300,h1,300,a1,300,g1,300,g1,200,a1,200,h1,200,a1,400,e1,200,fis1,600,d1,400,d1,200,e1,900,e1,300,c2,300,h1,300,a1,300,g1,300,d2,450,a1,150,a1,1200,d1,400,d1,200,e1,900,
//		e1,300,c2,300,h1,300,a1,300,g1,300,g1,200,a1,200,h1,200,a1,400,e1,200,fis1,600,d2,400,d2,200,g2,300,f2,300,es2,300,d2,300,c2,300,b1,300,a1,300,g1,300,d2,1800,d1,400,d1,200, 0,0};
uint8_t sound = 1;
void play_music(uint16_t music[])
{
	uint16_t czas = 1;
	uint16_t dzwiek = 1;
	uint16_t licznik = 0;

	while(dzwiek)
	{
		dzwiek = pgm_read_word(&music[licznik]);
		czas = pgm_read_word(&music[licznik+1]);

		generate_sound(dzwiek, czas);
		delay(czas);
		licznik+=2;
	}


}
void delayAnimate(uint8_t delay)
{
	while(delay--)
	{
		_delay_ms(10);
		if(check_buttons())
		{
			animationOn = 0;
			longjmp(buf,1);
		}
	}
}
void eti_animate(){
	animationOn = 1;
	while(1)
	{
		uint8_t n = 3;
		for (uint8_t i = 0; i < 7; i++)
		{
			frameToBufor_flash((const uint16_t*)&eti_zoom[i]);
			delayAnimate(10);
		}
		frameToBufor_flash(eti4);
		delayAnimate(50);
		while(n--)
		{
			frameToBufor_flash(eti0);
			delayAnimate(10);
			frameToBufor_flash(eti1);
			delayAnimate(10);
			frameToBufor_flash(eti2);
			delayAnimate(10);
			frameToBufor_flash(eti3);
			delayAnimate(10);
			frameToBufor_flash(eti4);
			delayAnimate(20);
			frameToBufor_flash(eti3);
			delayAnimate(10);
			frameToBufor_flash(eti2);
			delayAnimate(10);
			frameToBufor_flash(eti1);
			delayAnimate(10);

			frameToBufor_flash(eti0m);
			delayAnimate(10);
			frameToBufor_flash(eti1m);
			delayAnimate(10);
			frameToBufor_flash(eti2m);
			delayAnimate(10);
			frameToBufor_flash(eti3m);
			delayAnimate(10);
			frameToBufor_flash(eti4m);
			delayAnimate(20);
			frameToBufor_flash(eti3m);
			delayAnimate(10);
			frameToBufor_flash(eti2m);
			delayAnimate(10);
			frameToBufor_flash(eti1m);
			delayAnimate(10);
		}

		frameToBufor_flash(eti0);
		delayAnimate(10);
		frameToBufor_flash(eti1);
		delayAnimate(10);
		frameToBufor_flash(eti2);
		delayAnimate(10);
		frameToBufor_flash(eti3);
		delayAnimate(10);
		frameToBufor_flash(eti4);
		delayAnimate(20);

		for (int8_t i = 6; i >= 0; i--)
		{
			frameToBufor_flash((const uint16_t*)&eti_zoom[i]);
			delayAnimate(10);
		}

		static uint16_t blank[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

		frameToBufor(blank);
		delayAnimate(10);


		uint8_t i = 0;
		delayAnimate(50);
		while(i < 3)
		{
			uint16_t bufor[16];
			for(uint8_t x = 0 ; x < 16; x++)
			{
				bufor[x] = pgm_read_word(&chip[i][x]);
			}
			frameToBufor(bufor);
			delayAnimate(50);

			uint8_t step = 0;
			while (step  < 8)
			{
				for(uint8_t x = step; x < 16 - step; x++)
				{
					bufor[x] |= (1<<step);
					frameToBufor(bufor);
					delayAnimate(1);
				}
				for(uint8_t y = step; y < 16 - step; y++)
				{
					bufor[15-step] |= (1<<y);
					frameToBufor(bufor);
					delayAnimate(1);
				}
				for(uint8_t x = 15-step; x > step; x--)
				{
					bufor[x] |= (1<<(15-step));
					frameToBufor(bufor);
					delayAnimate(1);
				}
				for(uint8_t y = 15-step; y > step; y--)
				{
					bufor[step] |= (1<<y);
					frameToBufor(bufor);
					delayAnimate(1);
				}
				step++;
			}
			step = 0;
			i++;
			while (step  < 8)
			{
				for(uint8_t x = step; x < 16 - step; x++)
				{
					bufor[x] &= ((~(1<<step))| pgm_read_word(&chip[i][x]));
					frameToBufor(bufor);
					delayAnimate(1);
				}
				for(uint8_t y = step; y < 16 - step; y++)
				{
					bufor[15-step] &= (~(1<<y)) |  pgm_read_word(&chip[i][15-step]);
					frameToBufor(bufor);
					delayAnimate(1);
				}
				for(uint8_t x = 15-step; x > step; x--)
				{
					bufor[x] &= (~(1<<(15-step))) | pgm_read_word(&chip[i][x]);
					frameToBufor(bufor);
					delayAnimate(1);
				}
				for(uint8_t y = 15-step; y > step; y--)
				{
					bufor[step] &= (~(1<<y)) | pgm_read_word(&chip[i][step]);
					frameToBufor(bufor);
					delayAnimate(1);
				}
				step++;
			}
		}
		delayAnimate(50);
		chip_url_slide();
	}
}
void delay(uint16_t count)
{
	while(count--)
	{
		_delay_ms(1);
	}
}
struct menu_item {
	uint16_t * (* get_image)();
	void(* target_function)();
	struct menu_item* return_to;
};

const char digits[][4] PROGMEM  = {
		{62, 65, 65, 62},
		{4, 2, 127, 0},
		{98, 81, 73, 70},
		{34, 73, 73, 54},
		{24, 20, 18, 127},
		{79, 73, 73, 49},
		{62, 73, 73, 50},
		{1, 113, 9, 7},
		{54, 73, 73, 54},
		{38, 73, 73, 62}
};

volatile uint8_t button = 0;
volatile uint16_t sound_duration_timer;

const uint16_t* PROGMEM beep_screen(){
	if (sound) return beep_on;
	else return beep_off;
}

void EEPROM_write(unsigned int uiAddress, unsigned char ucData)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE));
	/* Set up address and Data Registers */
	EEAR = uiAddress;
	EEDR = ucData;
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);
}

unsigned char EEPROM_read(unsigned int uiAddress)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE))
		;
	/* Set up address register */
	EEAR = uiAddress;
	/* Start eeprom read by writing EERE */
	EECR |= (1<<EERE);
	/* Return data from Data Register */
	return EEDR;
}
void SPI_MasterInit(void)
{
	/* Enable SPI, Master, set clock rate fck/2 */
	SPCR = (1<<SPE)|(1<<MSTR); //DORD = 1 <- LSB First
	SPSR |= (1 << SPI2X);
}


void SPI_Send(uint8_t dataByte)
{
	PORTB &= ~ (1 << LATCH);
	SPDR = dataByte;
	while(!(SPSR & (1<<SPIF)));
	PORTB |= (1 << LATCH);
}

uint8_t check_buttons()
{

	if(~PIND & (1<<PD2))
	{
		return OK;
	}
	else if(~PINC & (1<<PC1))
	{
		return BPRAWY;
	}
	if(~PINC & (1<<PC2))
	{
		return BLEWY;
	}
	else if(~PINC & (1<<PC3))
	{
		return BGORA;
	}
	else if(~PINC & (1<<PC0))
	{
		return BDOL;
	}
	else if(~PINC & (1<<PC4))
	{
		return PAUZA;
	}
	return 0;
}

uint8_t menu()
{

	uint8_t menu_pos = 0;
	frameToBufor_flash(word_pong);
	_delay_ms(2000);
	while(button == OK);

	while(button != OK){

		if(menu_pos == 0)
		{
			if(button == BPRAWY)
			{
				menu_pos = 1;
				slide_right(word_pong, word_snake);
			}
		}
		else if(menu_pos == 1)
		{
			if(button == BLEWY)
			{
				menu_pos = 0;
				slide_left(word_pong, word_snake);

			}
			else if(button == BPRAWY)
			{
				menu_pos = 2;
				slide_right(word_snake, word_tetris);

			}
		}
		else if(menu_pos == 2)
		{
			if(button == BLEWY)
			{
				menu_pos = 1;
				slide_left(word_snake, word_tetris);

			}
			else if(button == BPRAWY)
			{
				menu_pos = 3;
				slide_right(word_tetris, battery);

			}
		}
		else if(menu_pos == 3)
		{
			if(button == BLEWY)
			{
				menu_pos = 2;
				slide_left(word_tetris, battery);

			}
			else if(button == BPRAWY)
			{
				menu_pos = 4;
				slide_right(battery, beep_screen());
			}
		}
		else if(menu_pos == 4)
		{
			smenu:
			if(button == BLEWY)
			{
				menu_pos = 3;
				slide_left(battery, beep_screen());
			}
		}

	}
	if(menu_pos == 4 ){

		button = 0;

		sound = 1 - sound;

		frameToBufor_flash(beep_screen());
		generate_sound(200,15);
		_delay_ms(210);
		goto smenu;

		//if (!sound) {
		//menu_pos = 31;
		//frameToBufor_flash(off);
		//}
		//else {
		//menu_pos=32;
		//frameToBufor_flash(on);
		//}
		//_delay_ms(500);
		//while(button != OK){
		//if(button == BPRAWY && menu_pos == 31)
		//{
		//menu_pos = 32;
		//sound = 1;
		//slide_right(off, on);
		//}
		//else if(button == BLEWY && menu_pos == 32)
		//{
		//menu_pos = 31;
		//sound = 0;
		//slide_left(off, on);
		//}
		//}
		//_delay_ms(500);


	}

	if(menu_pos == 1 ){
		frameToBufor_flash(play);
		button = 0;
		menu_pos = 11;
		_delay_ms(500);
		while(button != OK){
			if(button == BPRAWY && menu_pos == 11)
			{
				menu_pos = 12;
				slide_right(play, record);
			}
			else if(button == BLEWY && menu_pos == 12)
			{
				menu_pos = 11;
				slide_left(play, record);
			}
		}
	}

	return menu_pos;
}

void slide_right(uint16_t left_frame[], uint16_t right_frame[])
{
	uint16_t slider[35];
	uint16_t temp[16];
	//dodaj lewa klatke
	for(uint8_t i = 0; i < 16; i++)
	{
		slider[i] = pgm_read_word(&left_frame[i]);
	}
	//dodaje prawa ramke
	slider[16] = 0;
	slider[17] = 0;

	for(uint8_t i = 18; i < 35; i++)
	{
		slider[i] = pgm_read_word(&right_frame[i-18]);
	}

	for(uint8_t j = 0; j < 19; j++)
	{
		for(int i = 0; i< 16; i++)
		{
			temp[i] = slider[i+j];

		}
		frameToBufor(temp);
		_delay_ms(40);
	}
}

void slide_left(uint16_t left_frame[], uint16_t right_frame[])
{
	uint16_t slider[35];
	uint16_t temp[16];
	//dodaj lewa klatke
	for(uint8_t i = 0; i < 16; i++)
	{
		slider[i] = pgm_read_word(&left_frame[i]);
	}
	//dodaje prawa ramke
	slider[16] = 0;
	slider[17] = 0;

	for(uint8_t i = 18; i < 35; i++)
	{
		slider[i] = pgm_read_word(&right_frame[i-18]);
	}

	for(int j = 18; j >= 0; j--)
	{
		for(int i = 0; i< 16; i++)
		{
			temp[i] = slider[i+j];

		}
		frameToBufor(temp);
		_delay_ms(40);
	}
}
void display_score(uint8_t temp_rekord)

{


	uint16_t record_frame[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	uint8_t number[3];
	uint8_t counter = 0;
	while(temp_rekord > 0)
	{
		number[counter] = temp_rekord % 10;

		counter++;
		temp_rekord = temp_rekord/10;
	}
	if(counter == 1)
	{
		record_frame[6] |= pgm_read_byte(&digits[number[0]][0])<<4;
		record_frame[7] |= pgm_read_byte(&digits[number[0]][1])<<4;
		record_frame[8] |= pgm_read_byte(&digits[number[0]][2])<<4;
		record_frame[9] |= pgm_read_byte(&digits[number[0]][3])<<4;
	}
	else if(counter == 2)
	{
		record_frame[3] |= pgm_read_byte(&digits[number[1]][0])<<4;
		record_frame[4] |= pgm_read_byte(&digits[number[1]][1])<<4;
		record_frame[5] |= pgm_read_byte(&digits[number[1]][2])<<4;
		record_frame[6] |= pgm_read_byte(&digits[number[1]][3])<<4;

		record_frame[9] |= pgm_read_byte(&digits[number[0]][0])<<4;
		record_frame[10] |= pgm_read_byte(&digits[number[0]][1])<<4;
		record_frame[11] |= pgm_read_byte(&digits[number[0]][2])<<4;
		record_frame[12] |= pgm_read_byte(&digits[number[0]][3])<<4;
	}

	else if(counter == 3)
	{
		record_frame[1] |= pgm_read_byte(&digits[number[2]][0])<<4;
		record_frame[2] |= pgm_read_byte(&digits[number[2]][1])<<4;
		record_frame[3] |= pgm_read_byte(&digits[number[2]][2])<<4;
		record_frame[4] |= pgm_read_byte(&digits[number[2]][3])<<4;

		record_frame[6] |= pgm_read_byte(&digits[number[1]][0])<<4;
		record_frame[7] |= pgm_read_byte(&digits[number[1]][1])<<4;
		record_frame[8] |= pgm_read_byte(&digits[number[1]][2])<<4;
		record_frame[9] |= pgm_read_byte(&digits[number[1]][3])<<4;

		record_frame[11] |= pgm_read_byte(&digits[number[0]][0])<<4;
		record_frame[12] |= pgm_read_byte(&digits[number[0]][1])<<4;
		record_frame[13] |= pgm_read_byte(&digits[number[0]][2])<<4;
		record_frame[14] |= pgm_read_byte(&digits[number[0]][3])<<4;
	}

	frameToBufor(record_frame);
	button = 0;
	_delay_ms(500);
	while(button != OK);
	button = 0;
	_delay_ms(500);
}


void display_record () {
	cli();
	uint8_t temp_rekord = EEPROM_read(0);
	sei();
	display_score(temp_rekord);

}


void voltage()
{
	ADMUX |= (1<<REFS0) | (1<<ADLAR)| (1<<MUX0) | (1<<MUX2);
	ADCSRA|= (1<<ADEN) | (1<<ADSC) | (1<<ADPS0)| (1<<ADPS1)| (1<<ADPS2);
	while( !(ADCSRA & (1<<ADIF)));
	uint8_t adc = ADCH;
	uint16_t voltage = adc *47;
	voltage = voltage>>3;
	ADCSRA |= (1<<ADIF);
	ADCSRA &= ~((1<<ADEN) | (1<<ADSC));
	uint8_t number[3];
	sei();

	uint8_t counter = 3;
	uint16_t voltage_frame[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	while(counter--)
	{
		number[counter] = voltage % 10;

		voltage = voltage/10;
	}
	voltage_frame[0] |= pgm_read_byte(&digits[number[0]][0])<<4;
	voltage_frame[1] |= pgm_read_byte(&digits[number[0]][1])<<4;
	voltage_frame[2] |= pgm_read_byte(&digits[number[0]][2])<<4;
	voltage_frame[3] |= pgm_read_byte(&digits[number[0]][3])<<4;

	voltage_frame[5] |= (1<<10);

	voltage_frame[7] |= pgm_read_byte(&digits[number[1]][0])<<4;
	voltage_frame[8] |= pgm_read_byte(&digits[number[1]][1])<<4;
	voltage_frame[9] |= pgm_read_byte(&digits[number[1]][2])<<4;
	voltage_frame[10] |= pgm_read_byte(&digits[number[1]][3])<<4;

	voltage_frame[12] |= pgm_read_byte(&digits[number[2]][0])<<4;
	voltage_frame[13] |= pgm_read_byte(&digits[number[2]][1])<<4;
	voltage_frame[14] |= pgm_read_byte(&digits[number[2]][2])<<4;
	voltage_frame[15] |= pgm_read_byte(&digits[number[2]][3])<<4;

	frameToBufor(voltage_frame);
	button = 0;
	_delay_ms(500);
	while(button != OK);
	button = 0;
	_delay_ms(500);
}

void generate_sound(uint16_t period, uint16_t duration){
	if(sound == 1)
	{
		TCCR1B |= (1 << WGM12)|(1 << CS11)|(1 << CS10);
		OCR1A = period;
		sound_duration_timer = duration;
		TCCR1A |= (1 << COM1A0);
		TCNT1 = 0;
	}
}
void modify_sound_state (void){
	if (sound_duration_timer == 0) {
		TCCR1A &= ~(1 << COM1A0);
		PORTB &= ~ (1 << PB1);
	}
	else sound_duration_timer--;
}

void game_over_slide()
{
	uint16_t slide[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	for(uint8_t i = 0; i < 59; i++)
	{
		for(uint8_t j = 0; j < 16; j++)
		{
			slide[j] = pgm_read_byte(&game_over[j+i])<<4;
		}
		frameToBufor(slide);
		_delay_ms(60);
	}
}

void you_win_slide()
{
	uint16_t slide[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	for(uint8_t i = 0; i < 47; i++)
	{
		for(uint8_t j = 0; j < 16; j++)
		{
			slide[j] = pgm_read_byte(&win[j+i])<<4;
		}
		frameToBufor(slide);
		_delay_ms(60);
	}
}

void chip_url_slide()
{
	uint16_t slide[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	for(uint8_t i = 0; i < sizeof(chip_url)/2 - 16; i++)
	{
		for(uint8_t j = 0; j < 16; j++)
		{
			slide[j] = pgm_read_word(&chip_url[j+i]);
		}
		frameToBufor(slide);
		delayAnimate(6);
	}
}
