#include <inttypes.h>

#ifndef UART_H_
#define UART_H_
#define SCREEN_CLEAR "\x1B""[2J\r"
#define CURSOR_HIDE "\x1b""[?25l"
#define CURSOR_SHOW "\x1b""[?25h"
#define UART_BAUD 9600
#define __UBRR ((F_CPU+UART_BAUD*8UL) / (16UL*UART_BAUD)-1)


void uart_init(uint16_t ubrr);
void uart_transmit_char(  char data );
void uart_transmit_string(char * data);
void uart_transmit_long(uint32_t liczba, uint8_t base);
unsigned char uart_receive_char( void );

#endif /* UART_H_ */
