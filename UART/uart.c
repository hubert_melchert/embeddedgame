#include <stdlib.h>

#include <avr/io.h>
#include <avr/iom168.h>
#include "uart.h"
#include "../main_functions.h"
#include <inttypes.h>
#include <avr/interrupt.h>

uint8_t uart_bufor = 0;
void uart_init( uint16_t ubrr ){
	/* Set baud rate */
	UBRR0H = (uint8_t)(ubrr>>8);
	UBRR0L = (uint8_t)ubrr;
	/* Enable receiver and transmitter */
	DDRB |= (1<<PD1);
	DDRB &= ~(1<<PD0);
	UCSR0B |= (1<<RXEN0)|(1<<TXEN0);
	UCSR0B|= (1<<RXCIE0); //WLACZENIE PRZERWANA OTRZYMANIA DANYCH

}

ISR(USART_RX_vect)
{
	cli();
	uart_bufor = UDR0;
	uint16_t frame[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	frame[0] |= uart_bufor;
	frameToBufor(frame);
	sei();
}




void uart_transmit_char(  char data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = data;
}
void uart_transmit_string(char * data){
	while(*data) uart_transmit_char(*data++);
}

void uart_transmit_long(uint32_t liczba, uint8_t base){
	char bufor[17];
	ltoa(liczba, bufor, base);
	uart_transmit_string(bufor);
}

unsigned char uart_receive_char( void )
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) );
	/* Get and return received data from buffer */
	return UDR0;
}

