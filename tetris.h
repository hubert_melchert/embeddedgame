/*
 * tetris.h
 *
 *  Created on: 23-01-2015
 *      Author: Hubert
 */

#ifndef TETRIS_H_
#define TETRIS_H_
#include "main_functions.h"

typedef struct myBrick Brick;
void tetris();
void displayBrick();
void generateBrick();
void clearBrick();
void rotateBrick();
uint8_t checkCollision(uint8_t direction);
uint8_t checkFullLine();
void clearBoard();
#endif /* TETRIS_H_ */
