/*
 * pong.h
 *
 *  Created on: 01-12-2014
 *      Author: Hubert
 */

#ifndef PONG_H_
#define PONG_H_

#include "main_functions.h"
#include "UART/uart.h"

void pong();
void draw_paddle(uint16_t board[16], uint8_t x, uint8_t dlugosc, uint8_t gora_dol);
#endif /* PONG_H_ */
