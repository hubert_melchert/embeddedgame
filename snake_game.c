#include "snake_game.h"
#include "main_functions.h"



#define	LEWO 1
#define	PRAWO 2
#define	GORA 3
#define	DOL 4


uint16_t speed;
//SNAKE
uint8_t length;
uint8_t direction; //1- LEWO, 2-PRAWO, 3-GORA, 4- DOL
//x, y

//APPLE
uint8_t apple_x;
uint8_t apple_y;
uint8_t body_pos[2][200];
void snake()
{
	speed = 250;

	direction = GORA;
	length = 2;
	body_pos[0][0] = 8;
	body_pos[1][0] = 4;
	uint16_t licznik = 0;
	uint16_t board[16];
	uint8_t temp_direction = GORA;
	generate_apple();
	while(1)
	{
		if(button == BLEWY && direction != PRAWO)
		{
			button = 0;
			temp_direction = LEWO;
		}
		else if(button == BPRAWY && direction != LEWO)
		{
			button = 0;
			temp_direction = PRAWO;
		}
		else if(button == BGORA && direction != DOL)
		{
			button = 0;
			temp_direction = GORA;
		}
		else if(button == BDOL && direction != GORA)
		{
			button = 0;
			temp_direction = DOL;
		}
		_delay_ms(1);
		if(button == PAUZA)
		{
			frameToBufor_flash(pauza);
			button = 0;
			_delay_ms(500);
			while((PINC & (1<<PC4)));
			_delay_ms(500);
			frameToBufor(board);
			_delay_ms(1000);
		}

		licznik++;

		if(licznik > speed)
		{
			uint8_t n  = 16;
			while(n--) board[n] = 0;

			direction = temp_direction;
			move_snake();

			for(uint8_t i = 0; i < length; i++)
			{
				board[body_pos[0][i]] |= 1<<body_pos[1][i];
			}
			board[apple_x] |= (1<<apple_y);

			frameToBufor(board);
			if(check_collision() == 1)
			{
				game_over_slide();
				break;
			}
			licznik = 0;
		}
	}
	display_score(length);
	uint8_t temp_rekord = EEPROM_read(0);
	if(length > temp_rekord || temp_rekord == 0xFF)
	{
		EEPROM_write(0, length);
	}

}
uint8_t check_collision()
{
	uint8_t x = body_pos[0][0];
	uint8_t y = body_pos[1][0];
	for(int i = 1; i < length; i++)
	{
		if(x == body_pos[0][i] && y == body_pos[1][i])
		{
			return 1;
		}
	}
	return 0;
}

void generate_apple()
{
	uint8_t x, y;
	uint8_t test = 0;
	do
	{
		x = TCNT0&15;
		y = (TCNT1)&15;
		test = 0;
		for(uint8_t i = 0; i < length; i++)
		{
			if(x == body_pos[0][i] && y == body_pos[1][i])
			{
				test = 1;
				break;
			}
		}
	}while(test == 1);
	generate_sound(95,6); //30,8
	apple_x = x;
	apple_y = y;
}
void move_snake()
{

	for(uint8_t i = length - 1; i > 0; i--)
	{
		body_pos[0][i] = body_pos[0][i-1];

		body_pos[1][i] = body_pos[1][i-1];
	}
	switch(direction)
	{
	case LEWO:
		if(body_pos[0][0] == 0)
		{
			body_pos[0][0] = 15;
			break;
		}
		body_pos[0][0]--;
		break;
	case PRAWO:
		if(body_pos[0][0] == 15)
		{
			body_pos[0][0] = 0;
			break;
		}
		body_pos[0][0]++;
		break;
	case DOL:
		if(body_pos[1][0] == 15)
		{
			body_pos[1][0] = 0;
			break;
		}
		body_pos[1][0]++;
		break;
	case GORA:
		if(body_pos[1][0] == 0)
		{
			body_pos[1][0] = 15;
			break;
		}
		body_pos[1][0]--;
		break;
	}

	if(body_pos[0][0] == apple_x && body_pos[1][0] == apple_y){
		length++;

		uint8_t x = body_pos[0][length-2];
		uint8_t y = body_pos[1][length-2];

		uint8_t x2 = body_pos[0][length-3];
		uint8_t y2 = body_pos[1][length-3];


		int8_t diff_x = x2-x;
		int8_t diff_y = y2-y;

		switch(diff_x)
		{
		case 1:
			body_pos[0][length-1] = body_pos[0][length-2]-1;
			body_pos[1][length-1] = body_pos[1][length-2];
			break;
		case -1:
			body_pos[0][length-1] = body_pos[0][length-2]+1;
			body_pos[1][length-1] = body_pos[1][length-2];
			break;
		case 0:
			switch(diff_y)
			{
			case 1:
				body_pos[0][length-1] = body_pos[0][length-2];
				body_pos[1][length-1] = body_pos[1][length-2]-1;
				break;
			case -1:
				body_pos[0][length-1] = body_pos[0][length-2];
				body_pos[1][length-1] = body_pos[1][length-2]+1;
				break;
			}
			break;
		}

		generate_apple();
		if(speed > 140) speed -= 30;
	}
}

