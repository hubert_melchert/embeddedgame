#include "UART/uart.h"
#include "pong.h"
#include "main_functions.h"




void pong()
{
	uint8_t a_x = 5; //paletka dolna
	int8_t b_x = 5; // paletka gorna
	uint8_t length = 4;
	uint8_t ball_x;
	uint8_t ball_y;
	uint8_t licznik = 0;
	uint8_t licznik_bledu = 0;
	uint8_t kierunekY = 1; //1 DOL. 0 GORA
	uint8_t kierunekX = 1; //1 PRAWO 0 LEWO
	int8_t blad_zadany  = 0;
	int8_t blad_aktualny  = 0;
	int8_t odbicie  = 0;
	ball_x = TCNT0 & 15;
	kierunekX = TCNT0 & 1;
	kierunekY = TCNT0 & 1;
	if(kierunekY == 1)
	{
		ball_y = 2;
	}
	else ball_y = 13;
	uint8_t speed = 0;
	uint8_t a_punkty = 0;
	uint8_t b_punkty = 0;

	while(1)
	{

		b_x = ball_x - 2 + blad_aktualny;

		if(b_x < 0 ) b_x = 0;
		else if (b_x > 12) b_x = 12;

		uint16_t board[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

		for(uint8_t i = 0; i < length; i++)
		{
			board[i+a_x] |= (1<<15);
		}
		for(uint8_t i = 0; i < length; i++)
		{
			board[i+b_x] |= (1<<0);
		}

		if(button == BLEWY)
		{
			button = 0;
			if(a_x > 0)
			{
				a_x --;
			}
		}
		else if(button == BPRAWY)
		{
			button = 0;
			if(a_x < 16-length)
			{
				a_x ++;
			}
		}
		else if(button == PAUZA)
		{
			frameToBufor_flash(pauza);
			button = 0;
			_delay_ms(500);
			while((PINC & (1<<PC4)));
			_delay_ms(500);
			board[ball_x] |= (1<<ball_y);
			frameToBufor(board);

			_delay_ms(1000);
		}

		delay(30 - speed- ((a_punkty+b_punkty)>>2));
		licznik++;
		licznik_bledu++;
		if(licznik_bledu > 40)
		{
			licznik_bledu = 0;
			blad_aktualny = 0;
			blad_zadany = (TCNT0 & 6) - 3;
		}
		if(licznik > 2)
		{
			if(speed > 0)
			{
				speed-=3;
			}
			if(odbicie == 1 )
			{
				odbicie = 0;
				generate_sound(30,8);
			}
			if(blad_zadany < 0 && -blad_aktualny < -blad_zadany)
			{
				blad_aktualny--;
			}
			if(blad_zadany > 0 && blad_aktualny < blad_zadany)
			{
				blad_aktualny++;
			}
			licznik = 0;
			//ODBIJANIE PILKI W POZIOMIE
			if(kierunekX == 1)
			{
				if(ball_x < 15)
				{
					ball_x++;
				}
				else
				{
					generate_sound(200,15);
					kierunekX = 0;
				}

			}
			else
			{
				if(ball_x > 0)
				{
					ball_x--;
				}
				else
				{
					generate_sound(200,15);
					kierunekX = 1;
				}
			}
			///////////////////



			if(kierunekY == 1)
			{
				ball_y ++;

			}
			else
			{
				ball_y--;

			}


		}
		if(ball_y == 14)
		{
			if(ball_x >= a_x && ball_x < a_x+length)
			{
				kierunekY = 0 ;
				odbicie = 1;
				speed = 18;
			}
			else if(ball_x == (a_x-1) && kierunekX == 1)
			{
				odbicie = 1;
				kierunekX = 0;
				kierunekY = 0 ;
				speed = 18;
			}
			else if(ball_x == a_x+length && kierunekX == 0)
			{
				kierunekX = 1;
				kierunekY = 0 ;
				odbicie = 1;
				speed = 18;
			}
		}
		else if(ball_y == 1)
		{
			if(ball_x >= b_x && ball_x < b_x+length)
			{
				odbicie = 1;
				kierunekY = 1 ;
				speed = 18;

			}
			else if(ball_x == (b_x-1) && kierunekX == 1)
			{
				kierunekY = 1 ;
				kierunekX = 0;
				odbicie = 1;
				speed = 18;
			}
			else if(ball_x == b_x+length && kierunekX == 0)
			{
				kierunekY = 1 ;
				kierunekX = 1;
				odbicie = 1;
				speed = 18;
			}
		}
		board[ball_x] |= (1<<ball_y);
		frameToBufor(board);
		if(ball_y == 15 || ball_y == 0 )
		{
			if(ball_y == 15)
			{
				b_punkty++;
			}
			else if(ball_y == 0)
			{
				a_punkty++;
			}
			if(b_punkty == 10)
			{
				game_over_slide();
				break;
			}
			else if(a_punkty == 10)
			{
				you_win_slide();
				break;
			}
			uint16_t punkty[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
			punkty[6] = pgm_read_byte(&digits[b_punkty][0]) | (pgm_read_byte(&digits[a_punkty][0])<<9);
			punkty[7] = pgm_read_byte(&digits[b_punkty][1]) | (pgm_read_byte(&digits[a_punkty][1])<<9);
			punkty[8] = pgm_read_byte(&digits[b_punkty][2]) | (pgm_read_byte(&digits[a_punkty][2])<<9);
			punkty[9] = pgm_read_byte(&digits[b_punkty][3]) | (pgm_read_byte(&digits[a_punkty][3])<<9);
			_delay_ms(1000);
			frameToBufor(punkty);
			_delay_ms(2000);
			a_x = 5;
			ball_x = TCNT0 & 15;

			kierunekX = TCNT0 & 1;
			kierunekY = TCNT0 & 1;
			if(kierunekY == 1)
			{
				ball_y = 2;
			}
			else ball_y = 13;
		}
	}
}
