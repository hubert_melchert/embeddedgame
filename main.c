#include <avr/io.h>
#include <avr/iom168.h>
#include <util/delay.h>
#include <inttypes.h>
#include <avr/interrupt.h>
#include "main_functions.h"
#include "snake_game.h"
#include "pong.h"
#include "tetris.h"

volatile uint8_t debounce = 0;
volatile uint8_t prev_button = 0;
volatile uint16_t bufor[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
jmp_buf buf;
volatile uint16_t activityCounter = 0;

void frameToBufor(const uint16_t frame[]) {
	cli();
	uint8_t i = 16;
	while (i--) {
		bufor[i] = frame[i];
	}
	sei();
}

void frameToBufor_flash(const uint16_t frame[]) {

	cli();
	uint8_t i = 16;
	while (i--) {
		bufor[i] = pgm_read_word(&frame[i]);
	}
	sei();
}

ISR(TIMER0_OVF_vect ) {
	static uint8_t i;
	DDRB &= ~(1 << PB0);

	SPI_Send((uint8_t) ((1 << (15 - i)) & 0x00FF));
	SPI_Send((uint8_t) ((1 << (15 - i)) >> 8));
	SPI_Send(~(uint8_t) (bufor[i] >> 8));
	SPI_Send(~(uint8_t) (bufor[i] & 0x00FF));

	modify_sound_state();
	i++;
	if (i == 16) {
		i = 0;
	}
	debounce++;
	if (debounce > 20) {
		button = check_buttons();
		debounce = 0;
	}
	if (!animationOn)
		activityCounter++;

	if (button)
		activityCounter = 0;
	if (activityCounter > 20000) {
		eti_animate();
	}

	DDRB |= (1 << PB0);
	PORTB &= ~(1 << PB0); //OUTPUT ENABLE
}
int main() {

	DDRB = 0xFF;
	PORTB |= (1 << LATCH);

	//Buttons

	PORTC |= (1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3) | (1 << PC4);
	PORTD |= (1 << PD2);

	SPI_MasterInit();

	//Timer od multipleksacji
	TCCR0B |= (1 << CS01) | (1 << CS00);
	TIMSK0 |= (1 << TOIE0);
	ACSR |= (1 << ACD);

	sei();

	uint8_t select;
	if (!setjmp(buf)) {
		eti_animate();
	}

	while (1) {
		select = menu();

		if (select == 11) {
			snake();
		} else if (select == 12) {
			display_record();
		} else if (select == 2) {
			tetris();
		} else if (select == 3) {
			voltage();
		} else if (select == 0) {
			pong();
		}
	}

}

